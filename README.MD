# 1 Install necessary libraries

    sudo apt-install php7.4 php7.4-mbstring composer
    composer install

# 2 Run the code with "php index.php [filename]"
# 3 run tests with "composer tests"