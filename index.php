<?php

use Vinty\Discount\DiscountHandler;
use Vinty\Package\PackageFactory;
use Vinty\Package\PackagePriceHandler;
use Vinty\Package\PackagePrinter;
use Vinty\Package\PackageValidator;
use Vinty\Provider\ProviderManager;

require_once realpath("vendor/autoload.php");


// Some input variable validation
if(!array_key_exists(1, $argv)) {
    throw new Exception('You must provide file name!');
}

try {
    $file = new SplFileObject($argv[1], 'r');
} catch(Exception $e) {
    throw new Exception("File: '{$argv[1]}' cannot be opened");
}

// Initial variables
$packageFactory = PackageFactory::getIntance();
$providerManager = ProviderManager::getIntance();

$discountHandler = new DiscountHandler();
$packagePriceHandler = new PackagePriceHandler($providerManager);

// input file reading
foreach($file as $line) {
    $trimmed = trim($line);

    if(strlen($trimmed) === 0) {
        continue;
    }
    
    $package = $packageFactory->createFromString($line);

    $packageValidator = new PackageValidator(ProviderManager::getIntance(), $package);
    if(!$packageValidator->isValid($package)) {
        PackagePrinter::printPackageInputData($package);

        continue;
    }

    $packagePriceHandler->calculatePrice($package);
    $discountHandler->applyDiscount($package);

    PackagePrinter::printPackageData($package);
}