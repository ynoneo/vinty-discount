<?php

use PHPUnit\Framework\TestCase;
use Vinty\Package\PackageFactory;
use Vinty\Package\PackageValidator;
use Vinty\Provider\ProviderManager;

class PackageValidatorTest extends TestCase {
    private PackageFactory $packageFactory;
    private ProviderManager $providerManager;

    public function setUp():void {
        $this->packageFactory = PackageFactory::getIntance();

        $providerManagerMock = $this->createPartialMock(ProviderManager::class, ['getProviders']);
        $providerManagerMock
            ->method('getProviders')
            ->willReturn([
                'LP' => [
                    'L' => 1.50,
                    'M' => 4.90,
                    'S' => 6.90,
                ],
            ]);

        $this->providerManager = $providerManagerMock::getIntance();
    }

    public function testIsValid(): void {
        $package = $this->packageFactory->createFromString('2018-01-01 S LP');
        $validator = new PackageValidator($this->providerManager, $package);

        $this->assertTrue($validator->isValid());
    }

    public function testIsInvalid(): void {
        $package = $this->packageFactory->createFromString('2018-01-01 CUPRS');
        $validator = new PackageValidator($this->providerManager, $package);

        $this->assertNotTrue($validator->isValid());
    }
}