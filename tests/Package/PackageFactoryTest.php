<?php

use PHPUnit\Framework\TestCase;
use Vinty\Package\Package;
use Vinty\Package\PackageFactory;

class PackageFactoryTest extends TestCase {
    public function testGetInstance(): void {
        $factory = PackageFactory::getIntance();

        $this->assertInstanceOf(PackageFactory::class, $factory);
    }
    
    public function testPackageCreationFromString(): void {
        $factory = PackageFactory::getIntance();

        $package = $factory->createFromString('2018-01-01 S MR');

        $this->assertInstanceOf(Package::class, $package);
    }
}