<?php

use PHPUnit\Framework\TestCase;
use Vinty\Package\Package;
use Vinty\Package\PackageFactory;
use Vinty\Package\PackagePrinter;

class PackagePrinterTest extends TestCase {
    private PackageFactory $packageFactory;

    public function setUp(): void {
        $this->packageFactory = PackageFactory::getIntance();
    }

    public function testPrintPackageData(): void {
        $package = $this->packageFactory->createFromString('2018-01-01 S PR');
        $expected = '2018-01-01 S PR 0.00 -' . PHP_EOL;

        $this->expectOutputString($expected, PackagePrinter::printPackageData($package));
    }

    public function testPrintOriginalPackageData(): void {
        $package = $this->packageFactory->createFromString('2018-01-01 CUPRS');
        $expected = '2018-01-01 CUPRS Ignored' .PHP_EOL;

        $this->expectOutputString($expected, PackagePrinter::printPackageInputData($package));
    }
}