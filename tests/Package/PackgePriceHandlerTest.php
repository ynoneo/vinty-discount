<?php

use PHPUnit\Framework\TestCase;
use Vinty\Package\PackageFactory;
use Vinty\Package\PackagePriceHandler;
use Vinty\Provider\ProviderManager;

class PackgePriceHandlerTest extends TestCase {
    private PackageFactory $packageFactory;
    private PackagePriceHandler $priceHandler;
    private ProviderManager $providerManager;
    
    public function setUp(): void {
        $this->packageFactory = PackageFactory::getIntance();

        $providerManagerMock = $this
            ->createPartialMock(ProviderManager::class, ['getProviders']);
        $providerManagerMock
            ->method('getProviders')
            ->willReturn([
                'LP' => [
                    'L' => 1.50,
                    'M' => 4.90,
                    'S' => 6.90,
                ],
            ]);

        $this->providerManager = $providerManagerMock::getIntance();
        $this->priceHandler = new PackagePriceHandler($this->providerManager);
    }

    public function testCalculatePrice(): void {
        $providerName = 'LP';
        $providerSize = 'L';
        $package = $this->packageFactory->createFromString("2018-01-01 {$providerSize} {$providerName}");
        $provider = $this->providerManager->getProvider($providerName);

        $this->priceHandler->calculatePrice($package);

        $this->assertEquals($package->getPrice(), $provider[$providerSize]);
    }
}