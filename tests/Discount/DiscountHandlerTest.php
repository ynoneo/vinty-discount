<?php

use PHPUnit\Framework\TestCase;
use Vinty\Discount\Discounter\IDiscount;
use Vinty\Discount\DiscountHandler;
use Vinty\Package\PackageFactory;

class DiscountHandlerTest extends TestCase {
    private DiscountHandler $discountHandler;
    private PackageFactory $packageFactory;

    public function setUp(): void {
        $this->discountHandler = $this->createPartialMock(DiscountHandler::class, ['collectDiscounts']);
        $this->packageFactory = PackageFactory::getIntance();
    }

    public function testHaveNotReachedMonthLimit(): void {
        $package = $this->packageFactory->createFromString('2018-01-01 L LP');
        $haveReached = $this->discountHandler->haveReachedMonthLimit($package);

        $this->assertFalse($haveReached);
    }

    public function testApplyDiscount(): void {
        $package = $this->packageFactory->createFromString('2018-03-01 L LP');
        $package->setPrice(10);

        $this->discountHandler
            ->method('collectDiscounts')
            ->willReturn([$this->buildMockDiscounter(10)]);

        $this->discountHandler->applyDiscount($package);

        $this->assertEquals(10, $package->getDiscount());
        $this->assertEquals(0, $package->getPrice());
    }

    public function testHaveReachedMonthLimit(): void {
        $package = $this->packageFactory->createFromString('2018-03-01 L LP');
        
        $haveReached = $this->discountHandler->haveReachedMonthLimit($package);

        $this->assertTrue($haveReached);
    }

    public function testGetAvailableDiscountAmount(): void {
        $package = $this->packageFactory->createFromString('2018-02-01 L LP');
        $availableDiscount = $this->discountHandler->getAvailableDiscountAmount($package);

        $this->assertEquals(10, $availableDiscount);
    }

    private function buildMockDiscounter(float $discount): object {
        $mock = $this->getMockBuilder(IDiscount::class)->getMock();
        $mock
            ->method('isAvailabe')
            ->willReturn(true);

        $mock
            ->method('calculate')
            ->willReturn($discount);

        return $mock;
    }
}