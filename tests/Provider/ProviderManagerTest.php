<?php

use PHPUnit\Framework\TestCase;
use Vinty\Provider\ProviderManager;

class ProviderManagerTest extends TestCase {
    private ProviderManager $providerManager;

    public function setUp():void {
        $mock = $this->createPartialMock(ProviderManager::class, ['getProviders']);
        $mock
            ->method('getProviders')
            ->willReturn([
                'LP' => [
                    'L' => 1.50,
                    'M' => 4.90,
                    'S' => 6.90,
                ],
            ]
        );

        $this->providerManager = $mock::getIntance();
    }   

    public function testGetInstance(): void {
        $manager = ProviderManager::getIntance();

        $this->assertInstanceOf(ProviderManager::class, $manager);
    }
    
    public function testGetProviders(): void {
        $providers = $this->providerManager->getProviders();

        $this->assertIsArray($providers);
    }

    public function testGetCorrentProvider(): void {
        $provider = $this->providerManager->getProvider('LP');

        $this->assertIsArray($provider);
        $this->assertArrayHasKey('S', $provider);
        $this->assertArrayHasKey('M', $provider);
        $this->assertArrayHasKey('L', $provider);
    }

    public function testGetIncorrectProvider(): void {
        $provider = $this->providerManager->getProvider('randomName');

        $this->assertNull($provider);
    }

    public function testGetSizePriceCorrectProvider(): void {
        $price = $this->providerManager->getSizePrice('LP', 'S');

        $this->assertIsFloat($price);
    }

    public function testGetSizePriceInCorrectProvider(): void {
        $price = $this->providerManager->getSizePrice('randomName', 'MD');

        $this->assertNull($price);
    }

    public function testProviderExists(): void {
        $exists = $this->providerManager->providerExists('LP');

        $this->assertTrue($exists);
    }

    public function testProviderNotExists(): void {
        $exists = $this->providerManager->providerExists('randomProvider');

        $this->assertFalse($exists);
    }

    public function testProviderHasSize(): void {
        $hasSize = $this->providerManager->providerHasSize('LP', 'L');

        $this->assertTrue($hasSize);
    }

    public function testProviderHasNotSize(): void {
        $hasSize = $this->providerManager->providerHasSize('randomProvider', 'randomSize');

        $this->assertFalse($hasSize);
    }
}