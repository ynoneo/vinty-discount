<?php

namespace Vinty\Package;

class PackagePrinter {
    public static function printPackageData(Package $package): void {
        $str = "{$package->getDateAsString()} {$package->getSize()} {$package->getProvider()} ";
        $str .= number_format($package->getPrice(), 2) . ' ';

        if(!$package->getDiscount()) {
            $str .= '-';
        } else {
            $str .= number_format($package->getDiscount(), 2);
        }

        echo $str . PHP_EOL;
    }

    public static function printPackageInputData(Package $package): void {
        $str = '';

        foreach($package->getOriginData() as $data) {
            $str .= $data . ' ';
        }

        echo trim($str) .  " Ignored" . PHP_EOL;
    }
}