<?php

namespace Vinty\Package;

use Vinty\Provider\ProviderManager;

class PackagePriceHandler {
    private ProviderManager $providerManager;

    public function __construct(ProviderManager $providerManager) {
        $this->providerManager = $providerManager;
    }

    public function calculatePrice(Package $package) {
        if(!$this->providerManager->providerHasSize($package->getProvider(), $package->getSize())) {
            return;
        }

        $price = $this->providerManager->getSizePrice($package->getProvider(), $package->getSize());
        
        $package->setPrice($price);
    }
}
