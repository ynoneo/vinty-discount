<?php

namespace Vinty\Package;

use DateTime;

class Package {
    private ?array $originData = null;
    private ?string $size = null;
    private ?string $provider = null;
    private ?DateTime $date = null;
    private float $price = 0;
    private float $discount = 0;

    public function setOriginData(array $data): Package {
        $this->originData = $data;

        return $this;
    }

    public function getOriginData(): array {
        return $this->originData;
    }

    public function setSize(?string $size): Package {
        $this->size = $size;

        return $this;
    }

    public function getSize(): string {
        return $this->size;
    }

    public function setProvider(?string $provider): Package {
        $this->provider = $provider;

        return $this;
    }

    public function getProvider(): ?string {
        return $this->provider;
    }

    public function setDate(?DateTime $date): Package {
        $this->date = $date;

        return $this;
    }

    public function getDate(): ?DateTime {
        return $this->date;
    }

    public function getDateAsString(string $format = 'Y-m-d'): string {
        return $this->getDate()->format($format);
    }

    public function setPrice($price): Package {
        $this->price = $price;

        return $this;
    }

    public function getPrice(): float {
        return $this->price;
    }

    public function setDiscount(float $discount): Package {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscount(): float {
        return $this->discount;
    }
}