<?php

namespace Vinty\Package;

use DateTime;

class PackageFactory {
    private static ?PackageFactory $instance = null;

    private function __construct() {
    }

    public static function getIntance(): PackageFactory {
        if(!self::$instance) {
            self::$instance = new PackageFactory();
        }

        return self::$instance;
    }

    public function createFromString(string $dataString): Package {
        $data = preg_split('/\s+/', $dataString);
        
        return (new Package())
            ->setOriginData($data)
            ->setDate($this->extractDate(self::getFromData($data, 0)))
            ->setProvider(self::getFromData($data, 2))
            ->setSize(self::getFromData($data, 1));
    }

    private function extractDate(?string $date): ?DateTime {
        return DateTime::createFromFormat('Y-m-d', $date) ?: null;
    }

    private static function getFromData(array $data, $key): ?string {
        return array_key_exists($key, $data) ? $data[$key] : null;
    }
}