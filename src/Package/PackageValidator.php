<?php

namespace Vinty\Package;

use Vinty\Provider\ProviderManager;

class PackageValidator {
    private ProviderManager $providerManager;
    private Package $package;

    public function __construct(ProviderManager $providerManager, Package $package) {
        $this->providerManager = $providerManager;
        $this->package = $package;
    }

    public function isValid(): bool {
        if(!$this->package) {
            return false;
        }

        return $this->isDateSet() && 
            $this->isValidProvider() && 
            $this->isValidSize();
    }  

    private function isDateSet(): bool {
        return !!$this->package->getDate();
    }

    private function isValidProvider(): bool {
        return $this->providerManager->providerExists($this->package->getProvider());
    }

    private function isValidSize(): bool {
        $package = $this->package;

        return $this->providerManager->providerHasSize($package->getProvider(), $package->getSize());
    }
}