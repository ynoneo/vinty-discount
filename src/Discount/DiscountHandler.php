<?php

namespace Vinty\Discount;

use Vinty\Package\Package;
use Vinty\Discount\Discounter\LPLargePackageDiscount;
use Vinty\Discount\Discounter\SmallPackageDiscount;

class DiscountHandler {
    const MONTHLY_DISCOUNT_LIMIT = 10;

    private static array $discountAmounts = [];
    private static ?array $availableDiscounts = null;

    /**
     * Checks if month limit is reached
     */
    public function haveReachedMonthLimit(Package $package): bool {
        $key = $this->getDiscountKey($package);

        return array_key_exists($key, self::$discountAmounts) && self::$discountAmounts[$key] <= 0;
    }

    /**
     * Gets available discount for package month
     */
    public function getAvailableDiscountAmount(Package $package): float {
        $key = $this->getDiscountKey($package);

        if(!array_key_exists($key, self::$discountAmounts)) {
            self::$discountAmounts[$key] = self::MONTHLY_DISCOUNT_LIMIT;
        }

        return self::$discountAmounts[$key];
    }

    /**
     * Applies discount to package if available
     */
    public function applyDiscount(Package $package): void {
        if($this->haveReachedMonthLimit($package)) {
            return;
        }

        $discount = $this->getActualDiscount($package);
        $discountedPrice = $package->getPrice() - $discount;

        $package->setDiscount($discount);
        $package->setPrice($discountedPrice);

        $this->subtractFromAvailableDiscount($package, $discount);
    }

    /**
     * Gets all available discount objects
     */
    public function collectDiscounts(): array {
        return [      
            new LPLargePackageDiscount(), 
            new SmallPackageDiscount(),
        ];
    }

    /**
     * Gets calculated total discount for a package.
     */
    private function getActualDiscount(Package $package): float {
        $availableDiscount = $this->getAvailableDiscountAmount($package);
        $discount = $this->calculateDiscount($package);

        // Checks if calculated total discount is lower than available discount within month limit
        if($discount > $availableDiscount) {
            $discount = $availableDiscount;
        }
        
        $currentPrice = $package->getPrice();

        // Checks if discount is lower than price (you cannot discount more that a price at this point)
        if($discount > $currentPrice) {
            $discount = $currentPrice;
        } 

        return $discount;
    }

    private function subtractFromAvailableDiscount(Package $package, $discount): void {
        // substracts discount from available monthly discount
        $key = $this->getDiscountKey($package);
    
        self::$discountAmounts[$key] -= $discount;    
    }

    /**
     * Goes through each discount IDiscount ojbect and if available calculates discount
     */
    private function calculateDiscount(Package $package): float {
        $discounters = $this->getAvailableDiscounts();
        $discount = 0;
        
        foreach($discounters as $discounter) {
            if(!$discounter->isAvailabe($package)) {
                continue;
            }

            $discount += $discounter->calculate($package);
        }

        return $discount;
    }

    /**
     * Gets available discount objects for a product
     */
    private function getAvailableDiscounts() : array {
        if(self::$availableDiscounts === null) {
            self::$availableDiscounts = $this->collectDiscounts();
        }

        return self::$availableDiscounts;
    }

    private function getDiscountKey(Package $package): string {
        return $package->getDateAsString('Y-m');
    }
}