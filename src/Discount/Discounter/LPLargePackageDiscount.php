<?php

namespace Vinty\Discount\Discounter;

use Vinty\Package\Package;
use Vinty\Provider\ProviderManager;

class LPLargePackageDiscount implements IDiscount {
    const MONTHLY_AMOUNT_TO_DISCOUNT = 3;

    private static array $monthlyPackageAmounts = [];

    public function isAvailabe(Package $package): bool {
        return $package->getSize() === ProviderManager::PACKAGE_SIZE_L && $package->getProvider() === 'LP';
    }

    public function calculate(Package $package): float {
        $dateKey = $package->getDateAsString('Y-m');
        if(!array_key_exists($dateKey, self::$monthlyPackageAmounts)) {
            self::$monthlyPackageAmounts[$dateKey] = 0;
        }

        self::$monthlyPackageAmounts[$dateKey]++;
        if(self::$monthlyPackageAmounts[$dateKey] === 3) {
            self::$monthlyPackageAmounts[$dateKey] = 0;

            return $package->getPrice();
        }

        return 0;
    }
}