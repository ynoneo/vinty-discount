<?php

namespace Vinty\Discount\Discounter;

use Vinty\Package\Package;

interface IDiscount {
    public function calculate(Package $package): float;
    public function isAvailabe(Package $package): bool;
}