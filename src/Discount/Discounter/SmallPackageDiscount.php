<?php

namespace Vinty\Discount\Discounter;

use Vinty\Package\Package;
use Vinty\Provider\ProviderManager;

class SmallPackageDiscount implements IDiscount {
    public static ?ProviderManager $providerManager = null;

    public function __construct() {
        self::$providerManager = ProviderManager::getIntance();
    }

    public function isAvailabe(Package $package): bool {
        return $package->getSize() === ProviderManager::PACKAGE_SIZE_S;
    }

    public function calculate(Package $package): float {
        $cheapestPrice = self::$providerManager->getCheapestBySize($package->getSize());
        $price = $package->getPrice();

        return $price > $cheapestPrice ? $price - $cheapestPrice : 0;
    }
}