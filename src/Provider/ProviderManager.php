<?php

namespace Vinty\Provider;

class ProviderManager {
    const PACKAGE_SIZE_S = 'S';
    const PACKAGE_SIZE_M = 'M';
    const PACKAGE_SIZE_L = 'L';

    const PROVIDERS = [
        'LP' => [
            self::PACKAGE_SIZE_S => 1.50,
            self::PACKAGE_SIZE_M => 4.90,
            self::PACKAGE_SIZE_L => 6.90,
        ],
        'MR' => [
            self::PACKAGE_SIZE_S => 2,
            self::PACKAGE_SIZE_M => 3,
            self::PACKAGE_SIZE_L => 4,
        ]
    ];

    private static ?ProviderManager $instance = null;
    private ?array $providers = null;

    private function __construct() {
        $this->getProviders();
    }

    public static function getIntance(): ProviderManager {
        if(!self::$instance) {
            self::$instance = new ProviderManager();
        }

        return self::$instance;
    }

    public function getProviders(): array {
        if(!$this->providers) {
            $this->providers = self::PROVIDERS;
        }

        return $this->providers;
    }

    public function getProvider(?string $name): ?array {
        if(!array_key_exists($name, $this->providers)) {
            return null;
        }

        return $this->providers[$name];
    }

    public function getSizePrice(string $name, string $size): ?float {
        $provider = $this->getProvider($name);
        if(!$provider) {
            return null;
        }

        return array_key_exists($size, $provider) ? $provider[$size] : null;
    }

    public function providerExists($name): bool {
        return !!$this->getProvider($name);
    }

    public function providerHasSize($name, $size): bool {
        return !!$this->getSizePrice($name, $size);
    }

    public function getCheapestBySize(string $size): ?float {
        if(count(self::PROVIDERS) === 0) {
            return null;
        }

        $cheapestPrice = null;

        foreach($this->getProviders() as $provider) {
            if(!array_key_exists($size, $provider)) {
                continue;
            }

            $providerPrice = $provider[$size];

            if($cheapestPrice === null || $provider[$size] < $cheapestPrice) {
                $cheapestPrice = $providerPrice;
            }
        }

        return $cheapestPrice;
    }
}